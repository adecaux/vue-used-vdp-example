# vue-used-vdp-example

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```
### Test local page
```
To test, add a valid staging listingId like

http://localhost:8080/used_cars/vehicle-detail/ul1931792943/chevrolet/camaro
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```



### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
