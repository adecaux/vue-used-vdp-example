import Vue from 'vue'
import Vuex from 'vuex'
import App from './App.vue'
import router from './router'
import VueSanitize from 'vue-sanitize'

/* Import Assets */
import AxiosHelper from '@/assets/js/axios_helper.js'
import JSUtilsMixin from '@/assets/js/js_utils_mixin.js'
import BuildMMYSelector from '@/assets/js/mmyz_builder_mixin.js'

Vue.use(Vuex);
Vue.use(VueSanitize);
Vue.config.productionTip = false

const store = new Vuex.Store({
    // variables and collections here
    state : {
        zipcode : '90245'
    },
    // sychronous functions for changing state e.g. add, edit, delete
    mutations : {

    },
    // asynchronous functions that can call one or more mutation functions
    actions : {
    }
})

new Vue({
    router,
    store,
    mixins : [JSUtilsMixin, BuildMMYSelector],
    data () {
        return {
            publicPath : process.env.BASE_URL,
            CDCObjects: {
                storageName : 'CDCObjects',
                timestamp : 0,
                MMList : {
                    makes: {
                        new: [],
                        used: []
                    },
                    models: {
                        new: {},
                        used: {}
                    }
                }
            },
            anyUsedListingId: '',
            makeNameDenorm : '',
            modelNameDenorm : ''
        }
    },
    mounted() {
        this.addOrFetchStorage(false, () => {
            // if we got no MMList, let's fetch and build it
            let hasMMList = ('MMList' in this.CDCObjects) ? this.CDCObjects.MMList.makes.new.length : 0;
            if(hasMMList == 0) {
                this.fetchSimpleMakesAndModels();
            }
        });
    },
    methods : {
        addOrFetchStorage(_key = false, _data = []) {
            // Test for localStorage support
            let localStorageFeatureExist = false, storage;
            try {
                storage = window.localStorage;
                storage.setItem('testItem', 'testItem');
                storage.removeItem('testItem');
                localStorageFeatureExist = true;
            } catch(e) {
                console.log('Main.addOrFetchStorage:: No localStorage support');
            }
            //storage.removeItem('CDCObjects');
            const storageName = this.CDCObjects.storageName;
            if(localStorageFeatureExist) {
                // if storage is missing, create it
                if(!storage.getItem(storageName)) {
                    storage.setItem(storageName, JSON.stringify(this.CDCObjects));
                    console.log('Main.addOrFetchStorage:: Storage (re)created');
                }
                // if the storage object exists, so let's write to it
                if(_key !== false && _key !== '' && typeof _data !== 'function') {

                    let storageData = JSON.parse(storage.getItem(storageName));
                    if(storageData) {
                        // update our data at the key, then write it back to our storage
                        storageData[_key] = _data;
                        storage.setItem(storageName, JSON.stringify(storageData));
                        console.log('Main.addOrFetchStorage:: update storage at key "' + _key + '" with... ', storageData);
                    }
                    // else we simply fetch our storage object & write it to the common shared object
                }else {

                    let storageData = JSON.parse(storage.getItem(storageName));
                    if(storageData.MMList.makes.new.length != this.CDCObjects.MMList.makes.new.length) {
                        this.CDCObjects = storageData;
                        console.log('Main.addOrFetchStorage:: Storage fetched & added to "CDCObjects"');
                    }
                }
                // do we have a callback?
                if(typeof _data === 'function') {
                    _data();
                }
            }else {
                console.log('Main.addOrFetchStorage:: Storage feature not supported');
            }
        },

        addToMMList(_data = []) {
            let key = 'MMList';
            this.CDCObjects[key] = _data;
            // also update our storage
            this.addOrFetchStorage(key, _data);
        },

        /* Fetch all simple makes/models */
        fetchSimpleMakesAndModels(_showOnlyNewModels = false) {

            /* normally we'd do our usual ajax & response conditionals here (.then() & .catch()), but I abstracted it */
            AxiosHelper.callGetService('newcar/v2/getAllSimpleMakes', (response) => {
                if(response) {
                    this.buildMMList(response.makes);
                }

            }, { showOnlyNewModels : _showOnlyNewModels});

        },

        /* build both normalized & denormalized versions of makes/models */
        buildMMList(mmList) {

            // this object will exist before this method can even be called
            const storageName = 'CDCObjects', MMKey = 'MMList';
            var mmObj = window[storageName] = { };
            mmObj[MMKey] = {
                'makes': {
                    'new': [],
                    'used': []
                },
                'models': {
                    'new': {},
                    'used': {}
                }
            };
            // Populate it. We're going off the most comment structures fed by various service calls
            var isArray = Array.isArray(mmList);
            // If the list is the default makes[name:Acura,models:[{name:TLX}]]
            if(isArray && typeof mmList[0].name != 'undefined' && typeof mmList[0].models == 'object') {
                var l, m, y, ar, normMake, normModel, listCnt = mmList.length,
                    listCnt2, listCnt3, hasAcode, lastAvailYr = 0,
                    thisYrIterated = 0;
                    //lastYrIterated = 0;
                var newMakeAdded, usedMakeAdded, newModelAdded, usedModelAdded, checkIfMakeIsObj, checkIfMakeModelIsObj, instanceOfMake, instanceOfModel, instanceOfYear;
                // grab our MMList storage
                let MMListStorage = mmObj[MMKey];
                // start fresh with our makes
                if(MMListStorage.makes['new'].length > 0) {
                    MMListStorage.makes['new'] = [];
                }
                if(MMListStorage.makes['used'].length > 0) {
                    MMListStorage.makes['used'] = [];
                }
                // now we iterate and save our make/models to both new & used objects
                for(l = 0; l < listCnt; l++) {
                    ar = {};
                    instanceOfMake = mmList[l];
                    normMake = this.JSUtils.stringTool.normalize(instanceOfMake.name);
                    ar[normMake] = instanceOfMake.name;
                    newMakeAdded = usedMakeAdded = false;
                    listCnt2 = instanceOfMake.models.length;

                    for(m = 0; m < listCnt2; m++) {
                        instanceOfModel = instanceOfMake.models[m];
                        // add make to nc if available but also if there's any available preview years
                        if(!newMakeAdded && (instanceOfModel.isAvailableNew || instanceOfModel.previewYears.length > 0)) {
                            MMListStorage.makes['new'].push(ar);
                            MMListStorage.models['new'][normMake] = {};
                            newMakeAdded = true;
                        }
                        // always add make to uc
                        if(!usedMakeAdded) {
                            MMListStorage.makes['used'].push(ar);
                            MMListStorage.models['used'][normMake] = {};
                            usedMakeAdded = true;
                        }
                        normModel = this.JSUtils.stringTool.normalize(instanceOfModel.name);
                        // iterate over years
                        lastAvailYr = (instanceOfModel.year * 1);
                        thisYrIterated = 0;
                        newModelAdded = usedModelAdded = false;
                        if('years' in instanceOfModel) {
                            listCnt3 = instanceOfModel.years.length;
                            // keep a tally of model yrs sold out so we can flag models
                            let numbOfNewModelYears = 0,
                                numOfNewModelSoldOut = 0;

                            for(y = 0; y < listCnt3; y++) {
                                instanceOfYear = instanceOfModel.years[y];
                                thisYrIterated = (instanceOfYear['name'] * 1);
                                // add models to appropriate map based on if the model year is new vs used
                                hasAcode = (typeof instanceOfYear.baseAcode != 'undefined') ? instanceOfYear.baseAcode : '';
                                // add model year to nc if available or if it has a preview year
                                if(!newModelAdded && (instanceOfYear.isAvailableNew || instanceOfModel.previewYears.length > 0)) {
                                    MMListStorage.models['new'][normMake][normModel] = {
                                        name: instanceOfModel.name,
                                        acode: hasAcode,
                                        isSoldOut : false,
                                        hasAvailableNewModel : instanceOfYear.isAvailableNew,
                                        years: []
                                    };
                                    newModelAdded = true;
                                }
                                // always add model to uc but exclude years newer than last available year
                                if(!usedModelAdded && thisYrIterated <= lastAvailYr) {
                                    MMListStorage.models['used'][normMake][normModel] = {
                                        name: instanceOfModel.name,
                                        acode: hasAcode,
                                        isSoldOut : null,
                                        years: []
                                    };
                                    usedModelAdded = true;
                                }
                                if(instanceOfYear.isAvailableNew) {
                                    MMListStorage.models['new'][normMake][normModel]['years'].push({
                                        'year': instanceOfYear['name'],
                                        'baseAcode': instanceOfYear['baseAcode'],
                                        'isSoldOut': instanceOfYear['isSoldOut']
                                    });

                                    if(instanceOfYear.isAvailableNew) {
                                        numbOfNewModelYears ++;
                                    }

                                } else if(thisYrIterated <= lastAvailYr) {
                                    MMListStorage.models['used'][normMake][normModel]['years'].push({
                                        'year': instanceOfYear['name'],
                                        'baseAcode': instanceOfYear['baseAcode'],
                                        'isSoldOut': null
                                    });
                                }

                                if(instanceOfYear.isSoldOut == true) {
                                    numOfNewModelSoldOut ++;
                                }
                            }
                            // we mark our model as sold out if the number of new models equal 0 & there's at least 1 sold out
                            if(numOfNewModelSoldOut > 0 && numbOfNewModelYears == 0) {
                                // note: this will allow us to optinoally 'hide' models that are all sold out
                                if(normMake in MMListStorage.models['new']) {
                                    if(normModel in MMListStorage.models['new'][normMake]) {
                                        MMListStorage.models['new'][normMake][normModel]['isSoldOut'] = true;
                                    }
                                }
                            }
                        }
                        // Iterate over any possible Preview Years
                        checkIfMakeIsObj = typeof MMListStorage.models['new'][normMake];
                        checkIfMakeModelIsObj = (checkIfMakeIsObj != 'undefined') ? typeof MMListStorage.models['new'][normMake][normModel] : 'undefined';
                        if('previewYears' in instanceOfModel && Object.keys(MMListStorage.models['new']).length > 0 && checkIfMakeIsObj != 'undefined' && checkIfMakeModelIsObj != 'undefined') {
                            let p, previewCnt = instanceOfModel['previewYears'].length;
                            // if the key doesn't exist in our collection, let's add it
                            if(!('previewYears' in MMListStorage.models['new'][normMake][normModel])) {
                                MMListStorage.models['new'][normMake][normModel]['previewYears'] = [];
                            }
                            for(p = 0; p < previewCnt; p++) {
                                MMListStorage.models['new'][normMake][normModel]['previewYears'].push({
                                    'year': instanceOfModel['previewYears'][p],
                                    'baseAcode': 0
                                });
                            }
                        }
                    }
                }
                if(listCnt == 0) {
                    console.log('Main.buildMMList:: you passed in an empty list');
                    return false;
                } else {
                    this.addToMMList(mmObj[MMKey]);
                    return true;
                }
            } else {
                console.log('Main.buildMMList:: the provided list must be in this format: "makes[name:Acura,models:[{name:TLX}]]"');
                return false;
            }
        },
    },
    watch : {
        CDCObjects : {
            deep : true,
            handler() {
                console.log('Main.watch:: data "CDCObects" was updated');
            }
        }
    },
    el : '#app',
    template : '<App :CDCObjects="CDCObjects" :anyUsedListingId="anyUsedListingId" :makeNameDenorm="makeNameDenorm" :modelNameDenorm="modelNameDenorm" />',
    components : { App }
})