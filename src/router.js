import Vue from 'vue'
import Router from 'vue-router'
import UsedCarBuyPathListingsDetailPage from './components/UsedCarBuyPathListingsDetailPage.vue'

Vue.use(Router)

export default new Router({
    mode : 'history',
    base : process.env.VUE_APP_ROUTER_BASE_URL,
    routes : [
        {
            path : '/used_cars/vehicle-detail/:anyUsedListingId/:makeName/:modelName',
            name : 'usedCarBuyPathListingsDetailPage',
            component : UsedCarBuyPathListingsDetailPage,
            props : true
        }
    ]
})