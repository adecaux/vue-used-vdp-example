/**
 * Search confluence for 'BuildMMYSelector Object'
 * - dbolton | 2018-11-04
 */
const BuildMMYSelector = {
    methods : {
        BuildMMYSelector : function(wrpr, opts, JSUtils, MMList) {
            var _this = this;
            this.constant = {
                ref: 'BuildMMYSelector::',
                ncuc: 'new',
                /* determine which set to load new|used|both */
                ncucYears: 'all',
                /* determine which years to show new|used|ncuc|all */
                objects: {
                    wrapper: null,
                    make: null,
                    model: null,
                    year: null,
                    zipcode: null,
                    makeFlyout: null,
                    modelFlyout: null,
                    yearFlyout: null,
                    yearFlyoutPr: null,
                    yearFlyoutNc: null,
                    yearFlyoutUc: null
                },
                noSelectionText: {
                    make: 'Select Make',
                    model: 'Select Model',
                    year: 'Year'
                },
                yearFlyoutTitles: {
                    pr: 'Preview',
                    nc: 'Available New',
                    uc: 'Used Years'
                },
                selections: {
                    make: '',
                    model: '',
                    year: '',
                    zipcode: ''
                },
                presetViews: {
                    make : '',
                    model : '',
                    year : ''
                },
                makeFlyoutColumn: 5,
                modelFlyoutColumn: 5,
                modelFlyoutCreated: false,
                modelFlyoutNaturalSort: false,
                yearFlyoutCreated: false,
                flyoutCloseEventCreated: false,
                hideModelOnSoldOut: false,
                hidePreviewModelsInFlyout: false,
                /* unique cases for iPad */
                isIPad: (window.navigator.userAgent.toLowerCase().match(/ipad/i) !== null ? true : false),
                touchOrClickEvent: 'click'
            };
            opts = opts || {};
            if(!('dropdowns' in opts)) {
                opts['dropdowns'] = {};
            }
            if(!('classNames' in opts)) {
                opts['classNames'] = {};
            }
            if(!('defaultData' in opts)) {
                opts['defaultData'] = {};
            }
            if(!('presetViews' in opts)) {
                opts['presetViews'] = {};
            }
            if(!('noSelectionText' in opts)) {
                opts['noSelectionText'] = {};
            }
            if(!('callbacks' in opts)) {
                opts['callbacks'] = {};
            }
            /**
             * PUBLIC: close all opened flyouts (associated to wrapper element)
             * @return bool true for optional chaining
             */
            _this.closeFlyout = function() {
                var getFlyouts = _this.constant.objects.wrapper.getElementsByClassName('mmyzFlyout'),
                    flyoutCnt = getFlyouts.length;
                for(let i = 0; i < flyoutCnt; i++) {
                    getFlyouts[i].style.display = 'none';
                }
                return true;
            };
            /**
             * PUBLIC: clear flyout of data (less structure)
             * @param str flyout = make|model|year; flyout to clear
             */
            _this.clearFlyout = function(flyout) {
                
                if(flyout == 'year') {
                    let obj = _this.getDropdownTextObj('year');
                    if(_this.constant.objects.year && 'year' in opts.flyouts && obj) {
                        if(isSelectElement(flyout)) {
                            obj.options[0].value = '';
                            obj.options[0].text = _this.constant.noSelectionText.year;
                        } else {
                            obj.innerHTML = _this.constant.noSelectionText.year;
                        }
                        obj.setAttribute('data-selectedyear', '');
                        _this.constant.selections.year = '';
                        let c, ulCols = _this.constant.objects.yearFlyout.getElementsByClassName('mmyzULColumns');
                        for(c = 0; c < ulCols.length; c++) {
                            ulCols[c].innerHTML = '';
                        }
                    }
                } else if(flyout == 'model') {
                    let obj = _this.getDropdownTextObj('model');
                    if(_this.constant.objects.model && 'model' in opts.flyouts && obj) {
                        obj.setAttribute('data-selectedmodel', '');
                        if(isSelectElement(flyout)) {
                            obj.options[0].value = '';
                            obj.options[0].text = _this.constant.noSelectionText.model;
                        } else {
                            obj.innerHTML = _this.constant.noSelectionText.model;
                        }
                        _this.constant.selections.model = '';
                        _this.constant.objects.modelFlyout.getElementsByClassName('mmyzFlyoutInner')[0].innerHTML = '';
                        // clear year if available
                        if(_this.constant.objects.year && 'year' in opts.flyouts && _this.getDropdownTextObj('year')) {
                            _this.getDropdownTextObj('year').setAttribute('data-selectedmake', '');
                            _this.getDropdownTextObj('year').setAttribute('data-selectedmodel', '');
                            _this.getDropdownTextObj('year').setAttribute('data-selectedyear', '');
                        }
                    }
                } else if(flyout == 'make') {
                    let obj = _this.getDropdownTextObj('make');
                    if(_this.constant.objects.make && 'make' in opts.flyouts && obj) {
                        obj.setAttribute('data-selectedmake', '');
                        if(isSelectElement(flyout)) {
                            obj.options[0].value = '';
                            obj.options[0].text = _this.constant.noSelectionText.make;
                        } else {
                            obj.innerHTML = _this.constant.noSelectionText.make;
                        }
                        _this.constant.objects.modelFlyout.getElementsByClassName('mmyzFlyoutInner')[0].innerHTML = '';
                        // clear model & year if available
                        if(_this.constant.objects.model && 'model' in opts.flyouts && _this.getDropdownTextObj('model')) {
                            _this.getDropdownTextObj('model').setAttribute('data-selectedmake', '');
                        }
                        if(_this.constant.objects.year && 'year' in opts.flyouts && _this.getDropdownTextObj('year')) {
                            _this.getDropdownTextObj('year').setAttribute('data-selectedmake', '');
                        }
                    }
                }
            };
            /**
             * PUBLIC: highlight/unhighlight selected values
             * @param str dropTag = make|model|year|zipcode - dropdown to get
             */
            _this.highlightCell = function(dropTag, val) {
                var i, cells, dataAttribute, activeClass = 'mmyzFlyoutCellActive';
                if(_this.constant.objects.make && dropTag == 'make') {
                    cells = _this.constant.objects.makeFlyout.getElementsByClassName('mmyzFlyoutMakeName');
                    dataAttribute = 'data-make';
                } else if(_this.constant.objects.model && dropTag == 'model') {
                    cells = _this.constant.objects.modelFlyout.getElementsByClassName('mmyzFlyoutModelName');
                    dataAttribute = 'data-model';
                } else if(_this.constant.objects.year && dropTag == 'year') {
                    cells = _this.constant.objects.yearFlyout.getElementsByClassName('mmyzFlyoutYearName');
                    dataAttribute = 'data-year';
                    // else kill script here
                } else {
                    return false;
                }
        
                let cellCnt = cells.length;
                for(i = 0; i < cellCnt; i++) {
                    cells[i].classList.remove(activeClass);
                    if(cells[i].getAttribute(dataAttribute) == val) {
                        cells[i].classList.add(activeClass);
                    }
                }
            }
            /**
             * PUBLIC: get's text wrapper for specified dropdown
             * @param str dropTag = make|model|year|zipcode - dropdown to get
             * @return text wrapper as an object
             */
            _this.getDropdownTextObj = function(dropTag) {
                if(_this.constant.objects.make && dropTag == 'make') {
                    if(isSelectElement(dropTag)) {
                        return _this.constant.objects.make.getElementsByTagName('select')[0];
                    } else {
                        return _this.constant.objects.make.getElementsByClassName('mmyzSearchSelectedMake')[0];
                    }
                } else if(_this.constant.objects.model && dropTag == 'model') {
                    if(isSelectElement(dropTag)) {
                        return _this.constant.objects.model.getElementsByTagName('select')[0];
                    } else {
                        return _this.constant.objects.model.getElementsByClassName('mmyzSearchSelectedModel')[0];
                    }
                } else if(_this.constant.objects.year && dropTag == 'year') {
                    if(isSelectElement(dropTag)) {
                        return _this.constant.objects.year.getElementsByTagName('select')[0];
                    } else {
                        return _this.constant.objects.year.getElementsByClassName('mmyzSearchSelectedYear')[0];
                    }
                } else if(_this.constant.objects.zipcode && dropTag == 'zipcode') {
                    return _this.constant.objects.zipcode.getElementsByClassName('mmyzSearchSelectedZip')[0];
                } else {
                    return false;
                }
            };
            /**
             * PUBLIC: filter years based on provided make & model and populates/shows year flyout 
             * @param str mk = (opt); make name; else will attempt to fetch by available make object 
             * @param str md = (opt); model name; else will attempt to fetch by available model object 
             * @param mix cb = (opt); any callback after filtering and flyout displays 
             */
            _this.filterModelYears = function(mk, md, cb) {
                let isMake, isModel, isCallback;
                // If neither one of these exist, gracefully exit 
                if(!_this.constant.objects.yearFlyoutNc && !_this.constant.objects.yearFlyoutUc) {
                    return false;
                }
                // if a single argument is passed, test if it's a callback
                if(arguments.length == 1) {
                    if(typeof mk == 'function') {
                        isCallback = mk;
                    }
                    isMake = _this.constant.selections.make;
                    isModel = _this.constant.selections.model;
                } else {
                    isMake = (typeof mk == 'string' && mk !== '') ? mk : _this.constant.selections.model;
                    isModel = (typeof md == 'string' && md !== '') ? md : _this.constant.selections.model;
                    isCallback = cb;
                }
                // if we don't have a make & model, stop here 
                if(!isMake || !isModel) {
                    consoleLog('filterModelYears: no make and/or model found; aborted!');
                    return false;
                }
                // get any pre-selected/set year 
                let cellActive, activeYear = '';
                if(_this.constant.objects.yearFlyout) {
                    activeYear = (_this.constant.selections.year != '') ? _this.constant.selections.year : _this.getDropdownTextObj('year').getAttribute('data-selectedyear');
                }
                // isolate the model we need to iterate over 
                let i, items = '', 
                    prevYears = MMList.models.new[isMake][isModel].previewYears,
                    ncYears = MMList.models.new[isMake][isModel].years,
                    ucYears = MMList.models.used[isMake][isModel].years;
                // let's look for previewYears
                if(prevYears.length > 0) {
                    let i, items = '', prevYearsCnt = prevYears.length;
                    for(i = 0; i < prevYearsCnt; i++) {
                        items += '<li><a class="mmyzFlyoutYearName" data-acode="' + prevYears[i].baseAcode + '" data-year="' + prevYears[i].year + '">' + prevYears[i].year + '</a></li>';
                    }
                    _this.constant.objects.yearFlyoutPr.getElementsByClassName('mmyzULColumns')[0].innerHTML = '<ul>' + items + '</ul>';
                    _this.constant.objects.yearFlyoutPr.style.display = 'block';
                } else {
                    _this.constant.objects.yearFlyoutPr.style.display = 'none';
                }
                // do we have nc years? If not, hide this section
                if(ncYears.length > 0) {
                    for(i = 0; i < ncYears.length; i++) {
                        // highlight active year
                        cellActive = (activeYear == ncYears[i].year) ? 'mmyzFlyoutCellActive' : '';
                        //
                        items += '<li><a class="mmyzFlyoutYearName ' + cellActive + '" data-acode="' + ncYears[i].baseAcode + '" data-year="' + ncYears[i].year + '">' + ncYears[i].year + '</a></li>';
                    }
                    _this.constant.objects.yearFlyoutNc.getElementsByClassName('mmyzULColumns')[0].innerHTML = '<ul>' + items + '</ul>';
                    _this.constant.objects.yearFlyoutNc.style.display = 'block';
                } else {
                    _this.constant.objects.yearFlyoutNc.style.display = 'none';
                }
                // do we have any uc years? If not, hide this section
                if(ucYears.length > 0) {
                    items = '';
                    let modu, tot = ucYears.length;
                    let cols = (tot >= 6) ? 6 : tot;
                    let itemsPerRow = Math.ceil(tot / cols);
                    for(i = 0; i < tot; i++) {
                        modu = i % itemsPerRow;
                        if(modu == 0) {
                            items += '<ul>';
                        }
                        // highlight active year
                        cellActive = (activeYear == ucYears[i].year) ? 'mmyzFlyoutCellActive' : '';
                        //
                        items += '<li><a class="mmyzFlyoutYearName ' + cellActive + '" data-acode="' + ucYears[i].baseAcode + '" data-year="' + ucYears[i].year + '">' + ucYears[i].year + '</a></li>';
                        if(modu == (itemsPerRow - 1) || i == (tot - 1)) {
                            items += '</ul>';
                        }
                    }
        
                    _this.constant.objects.yearFlyoutUc.getElementsByClassName('mmyzULColumns')[0].innerHTML = items;
                    _this.constant.objects.yearFlyoutUc.style.display = 'block';
                } else {
                    _this.constant.objects.yearFlyoutUc.style.display = 'none';
                }
        
                if(typeof isCallback == 'function') {
                    isCallback(isMake, isModel);
                }
            };
            /**
             * PUBLIC: build model flyout and logic 
             * @param str mk = (opt); load models based on this; if not provided, will attempt to fetch via any associated make object
             */
            _this.buildModelFlyout = function(mk) {
                if(!_this.constant.objects.model) {
                    consoleLog(_this.constant.ref + 'buildModelFlyout: The Model Object is missing; aborted!');
                    return false;
                }
                // first get our selected make; stop here if selection is not made or not pre-set
                let isMake = '';
                if(typeof mk == 'string' && mk !== '') {
                    // is this a real make?
                    let hasMake;
                    if(_this.constant.ncuc == 'new') {
                        hasMake = MMList.makes.new;
                    } else if(_this.constant.ncuc == 'used') {
                        hasMake = MMList.makes.used
                    } else if(_this.constant.ncuc == 'both') {
                        hasMake = (MMList.makes.new).concat(MMList.makes.used);
                    }
                    let i, cnt = hasMake.length;
                    for(i = 0; i < cnt; i++) {
                        if(Object.keys(hasMake[i])[0] == mk) {
                            isMake = mk;
                            _this.constant.selections.make = mk;
                            break;
                        }
                    }
                } else {
                    isMake = _this.getDropdownTextObj('make');
                    isMake = (isMake) ? isMake.getAttribute('data-selectedmake') : '';
                }
                if(isMake == '') {
                    consoleLog(_this.constant.ref + 'buildModelFlyout: no make selected, passed OR not set to "data-selectedmake"');
                    return false;
                }
                // If flyout not already created, do so
                if(!_this.constant.modelFlyoutCreated) {
                    var flyout = document.createElement('div'),
                        flyoutInner = document.createElement('div');
                    flyout.setAttribute('class', 'mmyzFlyout ' + opts.flyouts.model);
                    flyoutInner.setAttribute('class', 'mmyzFlyoutInner');
                    flyout.appendChild(flyoutInner);
                    _this.constant.objects.model.appendChild(flyout);
                    _this.constant.objects.modelFlyout = flyout;
                }
        
                var model, modu, markup = '',
                    totalCnt = 0,
                    i = 0,
                    colNum = _this.constant.modelFlyoutColumn;
                // see if we have model years in this display instance; if not we determine if we need to hide certain models 
                let yearFlyoutExist = (_this.constant.objects.year) ? true : false;
                // at this point we know we have a make, let's fetch our models 
                var originalModels = MMList.models[_this.constant.ncuc][isMake];
                var models = JSON.parse(JSON.stringify(originalModels));
                // how many models do we have?
                for(model in models) {
                    if(_this.constant.hidePreviewModelsInFlyout && models[model].previewYears.length && !models[model].hasAvailableNewModel){
                        delete models[model];
                        continue;
                    }
                    totalCnt++;
                }
                let numPerCol = Math.ceil(totalCnt / colNum); /* how many items per column */
                let activeModel = (_this.constant.selections.model != '') ? _this.constant.selections.model : _this.getDropdownTextObj('model').getAttribute('data-selectedmodel');
                let cellActive;
                const createStructure = function(model, modu) {
                    var markup = '';
                    if(modu == 0) {
                        markup += '<ul>';
                    }
                    // if option is enabled, hide model on sold out
                    if(_this.constant.hideModelOnSoldOut) {
                        if(!yearFlyoutExist && models[model].isSoldOut) {
                            return false;
                        }
                    }
                    // highlight active model
                    cellActive = (activeModel == model) ? 'mmyzFlyoutCellActive' : '';
                    //
                    markup += '<li><a class="mmyzFlyoutModelName ' + cellActive + '" data-acode="' + models[model].acode + '" data-make="' + isMake + '" data-model="' + model + '" data-modeldenormalized="' + models[model].name + '">' + models[model].name + '</a></li>';
                    // close our ul if we met our limit per column or end of iteration
                    if(modu == (numPerCol - 1) || i == (totalCnt - 1)) {
                        markup += '</ul>';
                    }
                    return markup;
                };
                // do a natural sort - alphanumeric (i.e. 86, 4runner, camry, etc)
                if(_this.constant.modelFlyoutNaturalSort) {
                    for(model in models) {
                        modu = i % numPerCol;
                        
                        let hasMarkup = createStructure(model, modu);
                        if(hasMarkup !== false) {
                            markup += hasMarkup;
                            i++;
                        }
                    }
                    // else do a UTF-16 sort (default sort of 'sort()' & as done by the service layer)
                } else {
                    let k, keys = Object.keys(models);
                    keys.sort();
                    for(i = 0; i < keys.length; i++) {
                        model = keys[i];
                        modu = i % numPerCol;
        
                        let hasMarkup = createStructure(model, modu);
                        if(hasMarkup !== false) {
                            markup += hasMarkup;
                        }
                    }
                }
                if(!_this.constant.modelFlyoutCreated) {
                    _this.constant.objects.model.addEventListener('click', function(e) {
                        _this.closeFlyout();
                        removeBodyListener();
                        if(!_this.constant.flyoutCloseEventCreated) {
                            document.body.addEventListener(_this.constant.touchOrClickEvent, isFlyoutOpen, true);
                            _this.constant.flyoutCloseEventCreated = true;
                        }
                        flyout.style.display = 'block';
                    }, true);
        
                    if(markup != '') {
                        _this.constant.modelFlyoutCreated = true;
                        _this.constant.objects.model.getElementsByClassName('mmyzFlyoutInner')[0].innerHTML = markup;
                        // create event delegator
                        _this.constant.objects.model.getElementsByClassName('mmyzFlyoutInner')[0].addEventListener('click', function(e) {
                            let gotModel = captureSelection('model', e);
                            if(gotModel) {
                                _this.filterModelYears(_this.buildYearFlyout);
                            }
                        }, true);
                    }
                    // Else update with new data 
                } else {
                    if(markup != '') {
                        _this.constant.objects.modelFlyout.getElementsByClassName('mmyzFlyoutInner')[0].innerHTML = markup;
                    }
                }
            };
            /**
             * PUBLIC: build year flyout and logic 
             * @param str mk = (opt); make name; else will attempt to fetch by available make object 
             * @param str md = (opt); model name; else will attempt to fetch by available model object 
             */
            _this.buildYearFlyout = function(mk, md) {
                if(!_this.constant.objects.year) {
                    consoleLog(_this.constant.ref + 'buildYearDropdown: The Year Object is missing; aborted!');
                    return false;
                }
                // Do we hve any years to begin with?
                if(!checkForYears('both')) {
                    consoleLog(_this.constant.ref + 'buildYearDropdown: There were no years found in either New or Used JSON objects; aborted!');
                    return false;
                }
                // Build our structure if not exist 
                if(!_this.constant.yearFlyoutCreated) {
                    // Generate our flyout wrapper & create click event for dropdown & window event to close flyout 
                    var flyout = document.createElement('div'),
                        flyoutInner = document.createElement('div');
                    flyout.setAttribute('class', 'mmyzFlyout ' + opts.flyouts.year);
                    flyoutInner.setAttribute('class', 'mmyzFlyoutInner');
                    // 3. Create our 3 divs of Preivew, New & Used
                    var sectionP = document.createElement('div'),
                        sectionN = document.createElement('div'),
                        sectionU = document.createElement('div');
                    sectionP.setAttribute('class', 'mmyzYearFlyoutRow mmyzPreviewYearRow');
                    sectionN.setAttribute('class', 'mmyzYearFlyoutRow mmyzNewCarYearRow');
                    sectionU.setAttribute('class', 'mmyzYearFlyoutRow mmyzUsedCarYearRow');
                    sectionP.innerHTML = '<h5>' + _this.constant.yearFlyoutTitles.pr + '</h5><div class="mmyzULColumns"></div>';
                    sectionN.innerHTML = '<h5>' + _this.constant.yearFlyoutTitles.nc + '</h5><div class="mmyzULColumns"></div>';
                    sectionU.innerHTML = '<h5>' + _this.constant.yearFlyoutTitles.uc + '</h5><div class="mmyzULColumns"></div>';
                    // 4. Append everything
                    flyoutInner.appendChild(sectionP);
                    flyoutInner.appendChild(sectionN);
                    flyoutInner.appendChild(sectionU);
                    flyout.appendChild(flyoutInner);
                    _this.constant.objects.year.appendChild(flyout);
                    _this.constant.objects.yearFlyout = flyout;
                    _this.constant.objects.yearFlyoutPr = sectionP;
                    _this.constant.objects.yearFlyoutNc = sectionN;
                    _this.constant.objects.yearFlyoutUc = sectionU;
                }
        
                // let's attempt to populate if we have a make/model 
                let gotMake, gotModel;
                if(typeof mk == 'string' && mk !== '' && typeof md == 'string' && md !== '') {
                    gotMake = mk;
                    gotModel = md;
                } else if(_this.constant.selections.make != '' && _this.constant.selections.model != '') {
                    gotMake = _this.constant.selections.make;
                    gotModel = _this.constant.selections.model;
                } else if(_this.constant.objects.make && _this.constant.objects.model) {
                    // we don't care if this is a real make/model; it'll be filtered later
                    if(_this.getDropdownTextObj('make')?.getAttribute('data-selectedmake') != '' && _this.getDropdownTextObj('model').getAttribute('data-selectedmodel') != '') {
                        gotMake = _this.getDropdownTextObj('make')?.getAttribute('data-selectedmake');
                        gotModel = _this.getDropdownTextObj('model').getAttribute('data-selectedmodel');
                    }
                } else if('make' in opts.defaultData && 'model' in opts.defaultData) {
                    gotMake = opts.defaultData.make;
                    gotModel = opts.defaultData.model;
                }
        
                if(gotMake && gotModel) {
                    _this.filterModelYears(gotMake, gotModel);
                }
        
                if(!_this.constant.yearFlyoutCreated) {
                    _this.constant.objects.year.addEventListener('click', function(e) {
                        _this.closeFlyout();
                        removeBodyListener();
                        if(!_this.constant.flyoutCloseEventCreated) {
                            document.body.addEventListener(_this.constant.touchOrClickEvent, isFlyoutOpen, true);
                            _this.constant.flyoutCloseEventCreated = true;
                        }
                        // only show if we content 
                        let i, hasContent = false, getContent = flyout.getElementsByClassName('mmyzULColumns');
                        for(i = 0; i < getContent.length; i++) {
                            if(getContent[i].innerHTML != '') {
                                hasContent = true;
                                break;
                            }
                        }
                        if(hasContent) {
                            flyout.style.display = 'block';
                        }
                    }, true);
                    // create event delegator
                    _this.constant.objects.year.getElementsByClassName('mmyzFlyoutInner')[0].addEventListener('click', function(e) {
                        captureSelection('year', e);
                    }, true);
        
                    _this.constant.yearFlyoutCreated = true;
                }
            };
        
            /**
             * Eventhing below here are PRIVATE methods 
             */
        
            var removeBodyListener = function() {
                try {
                    document.body.removeEventListener(_this.constant.touchOrClickEvent, isFlyoutOpen, true);
                    _this.constant.flyoutCloseEventCreated = false;
                } catch(er) {}
            };
        
            var isFlyoutOpen = function(e) {
                var setToClose = true,
                    _node = e.target;
                while(_node && _node != null) {
                    if(_node == _this.constant.objects.wrapper) {
                        setToClose = false;
                        break;
                    }
                    _node = _node.parentNode;
                }
                if(setToClose) {
                    _this.closeFlyout();
                }
                // kill our body event 
                removeBodyListener();
            };
            /**
             * Capture selected (clicked) make, model or year value. Also reset some data attributes
             */
            var captureSelection = function(dropTag, e) {
                var setToClose = false,
                    obj, captured = '';
                switch(dropTag) {
                case 'make':
                    if(e.target.tagName.toLowerCase() == 'a' && e.target.classList.contains('mmyzFlyoutMakeName')) {
                        obj = _this.getDropdownTextObj('make');
                        captured = e.target.getAttribute('data-make');
                        if(isSelectElement(dropTag)) {
                            obj.options[obj.selectedIndex].setAttribute('value', captured);
                            obj.options[obj.selectedIndex].text = e.target.getAttribute('data-makedenormalized');
                        } else {
                            obj.innerHTML = e.target.getAttribute('data-makedenormalized');
                        }
                        obj.setAttribute('data-selectedmake', captured);
                        setToClose = true;
                        // Set flyout highlight 
                        _this.highlightCell('make', captured);
                        // if flyout was created, let's clear it 
                        if(_this.constant.modelFlyoutCreated && 'model' in opts.flyouts) {
                            _this.clearFlyout('model');
                        }
                        // Now try to (re)build our model flyout 
                        if(_this.constant.objects.model && 'model' in opts.flyouts) {
                            _this.buildModelFlyout();
                            _this.getDropdownTextObj('model').setAttribute('data-selectedmake', captured);
                        }
                        // Any year dropdown to write our make to?
                        if(_this.constant.yearFlyoutCreated && 'year' in opts.flyouts) {
                            _this.getDropdownTextObj('year').setAttribute('data-selectedmake', captured);
                        }
                        _this.constant.selections.make = captured;
                        // Clear our Year & flyout if exists
                        _this.clearFlyout('year');
    
                        // any callbacks ? 
                        if('make' in opts.callbacks) {
                            if(typeof opts.callbacks.make == 'function') {
                                opts.callbacks.make(e.target.getAttribute('data-make'), obj);
                            }
                        }
                    }
                    break;
                case 'model':
                    if(e.target.tagName.toLowerCase() == 'a' && e.target.classList.contains('mmyzFlyoutModelName')) {
                        obj = _this.getDropdownTextObj('model');
                        captured = e.target.getAttribute('data-model');
                        if(isSelectElement(dropTag)) {
                            obj.options[obj.selectedIndex].setAttribute('value', captured);
                            obj.options[obj.selectedIndex].text = e.target.getAttribute('data-modeldenormalized');
                        } else {
                            obj.innerHTML = e.target.getAttribute('data-modeldenormalized');
                        }
                        obj.setAttribute('data-selectedmodel', captured);
                        setToClose = true;
                        // Set flyout highlight 
                        _this.highlightCell('model', captured);
                        // any year dropdown to write our make to?
                        if(_this.constant.yearFlyoutCreated && 'year' in opts.flyouts) {
                            _this.getDropdownTextObj('year').setAttribute('data-selectedmodel', captured);
                        }
                        _this.constant.selections.model = captured;
                        // Clear our Year & flyout if exists
                        _this.clearFlyout('year');
    
                        // any callbacks ? 
                        if('model' in opts.callbacks) {
                            if(typeof opts.callbacks.model == 'function') {
                                opts.callbacks.model(captured, obj);
                            }
                        }
                    }
                    break;
                case 'year':
                    if(e.target.tagName.toLowerCase() == 'a' && e.target.classList.contains('mmyzFlyoutYearName')) {
                        obj = _this.getDropdownTextObj('year');
                        captured = e.target.getAttribute('data-year');
                        if(isSelectElement(dropTag)) {
                            obj.options[obj.selectedIndex].setAttribute('value', captured);
                            obj.options[obj.selectedIndex].text = captured;
                        } else {
                            obj.innerHTML = captured;
                        }
                        obj.setAttribute('data-selectedyear', captured);
                        setToClose = true;
                        _this.constant.selections.year = captured;
                        // Set flyout highlight 
                        _this.highlightCell('year', captured);
                        // any callbacks ? 
                        if('year' in opts.callbacks) {
                            if(typeof opts.callbacks.year == 'function') {
                                opts.callbacks.year(captured, obj);
                            }
                        }
                    }
                    break;
                }
                if(setToClose) {
                    removeBodyListener();
                    _this.closeFlyout();
                    // return true in case we want to proceed with subsequent logic 
                    return captured;
                } else {
                    return false;
                }
            };
            /**
             * Checks if any years exist in our list
             * @param str ncuc = new|used|both; if both, will return bool true if years exist for both new & used
             */
            var checkForYears = function(ncuc) {
                ncuc = (typeof ncuc == 'string' && ncuc !== '') ? ncuc : 'new';
                let hasYears = false, tmpList = MMList;
                if(typeof tmpList.models == 'object') {
                    if(ncuc == 'both') {
                        let firstNCVal = Object.values(tmpList.models['new'])[0], firstUCVal = Object.values(tmpList.models['used'])[0];
    
                        firstNCVal = (firstNCVal) ? Object.values(firstNCVal)[0] : false;
                        firstUCVal = (firstUCVal) ? Object.values(firstUCVal)[0] : false;
                        if(firstNCVal && firstUCVal) {
                            hasYears = true;
                        }
                    } else {
                        let firstVal = Object.values(Object.values(tmpList.models[ncuc])[0])[0];
                        if(firstVal) {
                            if(firstVal.years) {
                                hasYears = true;
                            }
                        }
                    }
                }
                return hasYears;
            };
            /**
             * Build make flyout and logic; this also triggers the buildModelFlyout if any 
             */
            var buildMakeFlyout = function() {
                if(!_this.constant.objects.make) {
                    consoleLog(_this.constant.ref + 'buildMakeFlyout: The Make Object is missing; aborted!');
                    return false;
                }
                // Generate our flyout wrapper & create click event for dropdown & window event to close flyout 
                var flyout = document.createElement('div'),
                    flyoutInner = document.createElement('div');
                flyout.setAttribute('class', 'mmyzFlyout ' + opts.flyouts.make);
                flyoutInner.setAttribute('class', 'mmyzFlyoutInner');
                flyout.appendChild(flyoutInner);
                _this.constant.objects.make.appendChild(flyout);
                _this.constant.objects.makeFlyout = flyout;
    
                var i, modu, objKey, objVal, markup = '',
                    colNum = _this.constant.makeFlyoutColumn,
                    cellActive, itemCnt = MMList.makes[_this.constant.ncuc].length;
                let numPerCol = Math.ceil(itemCnt / colNum); /* how many items per column */
                let activeMake = (_this.constant.selections.make != '') ? _this.constant.selections.make : _this.getDropdownTextObj('make')?.getAttribute('data-selectedmake');
                for(i = 0; i < itemCnt; i++) {
                    modu = i % numPerCol;
                    if(modu == 0) {
                        markup += '<ul>';
                    }
                    objKey = Object.keys(MMList.makes[_this.constant.ncuc][i]);
                    objVal = MMList.makes[_this.constant.ncuc][i][objKey];
                    // highlight active make
                    cellActive = (activeMake == objKey) ? 'mmyzFlyoutCellActive' : '';
                    //
                    markup += '<li><a class="mmyzFlyoutMakeName ' + cellActive + '" data-make="' + objKey + '" data-makedenormalized="' + objVal + '">' + objVal + '</a></li>';
                    // close our ul if we met our limit per column or end of iteration
                    if(modu == (numPerCol - 1) || i == (itemCnt - 1)) {
                        markup += '</ul>';
                    }
                }
                _this.constant.objects.make.addEventListener('click', function(e) {
                    _this.closeFlyout();
                    removeBodyListener();
                    if(!_this.constant.flyoutCloseEventCreated) {
                        document.body.addEventListener(_this.constant.touchOrClickEvent, isFlyoutOpen, true);
                        _this.constant.flyoutCloseEventCreated = true;
                    }
                    flyout.style.display = 'block';
                }, true);
    
                if(markup != '') {
                    _this.constant.objects.make.getElementsByClassName('mmyzFlyoutInner')[0].innerHTML = markup;
                    // create event delegator
                    _this.constant.objects.make.getElementsByClassName('mmyzFlyoutInner')[0].addEventListener('click', function(e) {
                        captureSelection('make', e);
                    }, true);
                    // Now try to build our model flyout if exists
                    if(_this.constant.objects.model && 'model' in opts.flyouts) {
                        _this.buildModelFlyout();
                    }
                }
    
                // any callbacks ? 
                if('selectMakes' in opts.callbacks) {
                    if(typeof opts.callbacks.selectMakes == 'function') {
                        opts.callbacks.selectMakes(_this.constant.objects.make);
                    }
                }
            };
            /**
             * Check if dropdown is a custom element or native select 
             * @param str elem = make|model|year to reference
             * @return bool
             */
            var isSelectElement = function(elem) {
                if(typeof elem == 'undefined') {
                    return false;
                }
                let isSelect = false;
                if(_this.constant.objects[elem]) {
                    if(_this.constant.objects[elem].firstChild.tagName) {
                        if(_this.constant.objects[elem].firstChild.tagName.toLowerCase() == 'select') {
                            isSelect = true;
                        }
                    }
                }
                return isSelect;
            };
        
            // Inherit JSUtil's logging method
            var consoleLog = JSUtils.consoleLog;
        
            let _init = (function() {
                // get our parts
                if(typeof wrpr == 'undefined' || wrpr == '') {
                    consoleLog(_this.constant.ref + ' Reference to the search wrapper wasn\'t provided, must pass 1st argument as a css class name into instantiation; aborted!');
                    return false;
                } else {
                    _this.constant.objects.wrapper = document.getElementsByClassName(wrpr)[0];
                }
                if(typeof _this.constant.objects.wrapper == 'undefined') {
                    consoleLog(_this.constant.ref + ' The "' + wrpr + '" wrapper doesn\'t exist; aborted!');
                    return false;
                }
                // set this for the poor iPad 
                _this.constant.touchOrClickEvent = (_this.constant.isIPad) ? 'touchend' : 'click';
                // Set some elements if they exist 
                var parentWrap;
                let spanWrap;
                if('make' in opts.classNames) {
                    _this.constant.objects.make = _this.constant.objects.wrapper.getElementsByClassName(opts.classNames.make)[0];
                    if(_this.constant.objects.make) {
                        // any default?
                        if('make' in opts.defaultData) {
                            _this.constant.objects.make.setAttribute('data-default', opts.defaultData.make);
                            _this.constant.selections.make = opts.defaultData.make;
                        } else {
                            // if it doesn't exist, create it
                            if(!_this.constant.objects.make.getAttribute('data-default')) {
                                _this.constant.objects.make.setAttribute('data-default', '');
                            } else {
                                _this.constant.selections.make = _this.constant.objects.make.getAttribute('data-default');
                            }
                        }
                        if('make' in opts.presetViews) {
                            _this.constant.presetViews.make = opts.presetViews.make;
                        }
                        // any no-selection default text?
                        if('make' in opts.noSelectionText) {
                            _this.constant.noSelectionText.make = opts.noSelectionText.make;
                        }
                        // Now see if we need to wrap our make search's text in a special span 
                        _this.constant.selections.make = _this.constant.objects.make.getAttribute('data-default');
                        // Let's do something special for select elements...
                        if(_this.constant.objects.make.tagName.toLowerCase() == 'select') {
                            _this.constant.objects.make.setAttribute('data-selectedMake', _this.constant.selections.make);
                            _this.constant.objects.make.style.pointerEvents = 'none';
        
                            parentWrap = document.createElement('div');
                            parentWrap.setAttribute('class', 'mmyzSearchSelectedMakeWrapper');
        
                            _this.constant.objects.make.parentNode.insertBefore(parentWrap, _this.constant.objects.make.nextSibling);
                            parentWrap.appendChild(_this.constant.objects.make);
                            // now the make obj becomes the parent wrapper that we created
                            _this.constant.objects.make = parentWrap;
                        } else {
                            spanWrap = document.createElement('span');
                            spanWrap.setAttribute('class', 'mmyzSearchSelectedMake');
                            spanWrap.setAttribute('data-selectedMake', _this.constant.selections.make);
                            spanWrap.innerHTML = (_this.constant.presetViews.make != '') ? _this.constant.presetViews.make : _this.constant.objects.make.innerHTML;
                            _this.constant.objects.make.innerHTML = '';
                            _this.constant.objects.make.appendChild(spanWrap);
                        }
                    } else {
                        console.log(_this.constant.ref + ' The make reference of "' + opts.classNames.make + '" was passed but element was not found! Make was not built; aborted');
                    }
                }
                if('model' in opts.classNames) {
                    _this.constant.objects.model = _this.constant.objects.wrapper.getElementsByClassName(opts.classNames.model)[0];
                    if(_this.constant.objects.model) {
                        // any default?
                        if('model' in opts.defaultData) {
                            _this.constant.objects.model.setAttribute('data-default', opts.defaultData.model);
                            _this.constant.selections.model = opts.defaultData.model;
                        } else {
                            if(!_this.constant.objects.model.getAttribute('data-default')) {
                                _this.constant.objects.model.setAttribute('data-default', '');
                            } else {
                                _this.constant.selections.model = _this.constant.objects.model.getAttribute('data-default');
                            }
                        }
                        if('model' in opts.presetViews) {
                            _this.constant.presetViews.model = opts.presetViews.model;
                        }
                        // any no-selection default text?
                        if('model' in opts.noSelectionText) {
                            _this.constant.noSelectionText.model = opts.noSelectionText.model;
                        }
                        // See if we need to wrap our model search's text in a special span or handle a select element
                        // Let's do something special for select elements...
                        if(_this.constant.objects.model.tagName.toLowerCase() == 'select') {
                            _this.constant.objects.model.setAttribute('data-selectedModel', _this.constant.selections.model);
                            _this.constant.objects.model.style.pointerEvents = 'none';
        
                            parentWrap = document.createElement('div');
                            parentWrap.setAttribute('class', 'mmyzSearchSelectedModelWrapper');
        
                            _this.constant.objects.model.parentNode.insertBefore(parentWrap, _this.constant.objects.model.nextSibling);
                            parentWrap.appendChild(_this.constant.objects.model);
                            // now the model obj becomes the parent wrapper that we created
                            _this.constant.objects.model = parentWrap;
                        } else {
                            spanWrap = document.createElement('span');
                            spanWrap.setAttribute('class', 'mmyzSearchSelectedModel');
                            spanWrap.setAttribute('data-selectedModel', _this.constant.objects.model.getAttribute('data-default'));
                            spanWrap.innerHTML = (_this.constant.presetViews.model !='') ? _this.constant.presetViews.model : _this.constant.objects.model.innerHTML;
                            _this.constant.objects.model.innerHTML = '';
                            _this.constant.objects.model.appendChild(spanWrap);
                        }
                    } else {
                        console.log(_this.constant.ref + ' The model reference of "' + opts.classNames.model + '" was passed but element was not found! Model was not built; aborted');
                    }
                }
                if('year' in opts.classNames) {
                    _this.constant.objects.year = _this.constant.objects.wrapper.getElementsByClassName(opts.classNames.year)[0];
                    if(_this.constant.objects.year) {
                        // any default?
                        if('year' in opts.defaultData) {
                            _this.constant.objects.year.setAttribute('data-default', opts.defaultData.year);
                            _this.constant.selections.year = opts.defaultData.year;
                        } else {
                            if(!_this.constant.objects.year.getAttribute('data-default')) {
                                _this.constant.objects.year.setAttribute('data-default', '');
                            } else {
                                _this.constant.selections.year = _this.constant.objects.year.getAttribute('data-default');
                            }
                        }
                        // any no-selection default text?
                        if('year' in opts.noSelectionText) {
                            _this.constant.noSelectionText.year = opts.noSelectionText.year;
                        }
                        // See if we need to wrap our year search's text in a special span or handle a select element
                        // Let's do something special for select elements...
                        if(_this.constant.objects.year.tagName.toLowerCase() == 'select') {
                            _this.constant.objects.year.setAttribute('data-selectedYear', _this.constant.selections.year);
                            _this.constant.objects.year.style.pointerEvents = 'none';
        
                            parentWrap = document.createElement('div');
                            parentWrap.setAttribute('class', 'mmyzSearchSelectedYearWrapper');
        
                            _this.constant.objects.year.parentNode.insertBefore(parentWrap, _this.constant.objects.year.nextSibling);
                            parentWrap.appendChild(_this.constant.objects.year);
                            // now the year obj becomes the parent wrapper that we created
                            _this.constant.objects.year = parentWrap;
                        } else {
                            spanWrap = document.createElement('span');
                            spanWrap.setAttribute('class', 'mmyzSearchSelectedYear');
                            spanWrap.setAttribute('data-selectedYear', _this.constant.objects.year.getAttribute('data-default'));
                            spanWrap.innerHTML = _this.constant.objects.year.innerHTML;
                            _this.constant.objects.year.innerHTML = '';
                            _this.constant.objects.year.appendChild(spanWrap);
                        }
                    } else {
                        console.log(_this.constant.ref + ' The year reference of "' + opts.classNames.year + '" was passed but element was not found! Years were not built; aborted');
                    }
                }
                if('zipcode' in opts.classNames) {
                    _this.constant.objects.zipcode = _this.constant.objects.wrapper.getElementsByClassName(opts.classNames.zipcode)[0];
                    // any default?
                    if('zipcode' in opts.defaultData) {
                        _this.constant.objects.zipcode.setAttribute('data-default', opts.defaultData.zipcode);
                    } else {
                        _this.constant.objects.zipcode.setAttribute('data-default', '');
                    }
                }
                // are we populating new car, used car or both?
                if('ncuc' in opts) {
                    if(opts.ncuc == 'used' || opts.ncuc == 'both') {
                        _this.constant.ncuc = opts.ncuc;
                    }
                }
                // what years are we showing new, used, ncuc (new & used) or all (preview, new & used)?
                if('ncucYears' in opts) {
                    _this.constant.ncucYears = opts.ncucYears;
                }
                // should we hide models on sold out? 
                if('hideModelOnSoldOut' in opts && typeof opts.hideModelOnSoldOut == 'boolean') {
                    _this.constant.hideModelOnSoldOut = opts.hideModelOnSoldOut;
                }
                // do we need to hide the preview models in model flyout? 
                if('hidePreviewModelsInFlyout' in opts && typeof opts.hidePreviewModelsInFlyout == 'boolean') {
                    _this.constant.hidePreviewModelsInFlyout = opts.hidePreviewModelsInFlyout;
                }
                // do we have a flag for model natural sort?
                if('modelFlyoutNaturalSort' in opts && typeof opts.modelFlyoutNaturalSort == 'boolean') {
                    _this.constant.modelFlyoutNaturalSort = opts.modelFlyoutNaturalSort;
                }
                // fetch or build our list
                //JSUtils.buildMMList(_this.constant.ncuc, '', function() {
                    // if we have a make dropdown, chances are we'll have a model so we attempt to build that too
                    if(_this.constant.objects.make && 'make' in opts.flyouts) {
                        buildMakeFlyout();
                    // else if no make flyout BUT we have a default make passed in AND we have a model flyout, attempt to build our model flyout 
                    } else if('make' in opts.defaultData && _this.constant.objects.model && 'model' in opts.flyouts) {
                        _this.buildModelFlyout(opts.defaultData.make);
                    }
                    // In this instant, we're just building out the structure
                    if(_this.constant.objects.year && 'year' in opts.flyouts) {
                        _this.buildYearFlyout();
                    }
                //});
                // Finally run callback if any 
                if('loaded' in opts.callbacks) {
                    opts.callbacks.loaded(_this);
                }
            })();
        
        }
    },
    mounted() {
        
    },
    watch : {
         '$root.CDCObjects' : {
            deep : true,
            handler() {
                
            }
         }
    }
};

export default BuildMMYSelector;