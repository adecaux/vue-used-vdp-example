import axios from "axios";
axios.defaults.baseURL = process.env.VUE_APP_AXIOS_BASE_URL;

const AxiosHelper = {
    /**
     * Abstracted method to call any service
     * @param str _service:  service to call; i.e. 'newcar/v2/getAllSimpleMakes'
     * @param mix _cb:  function callback; triggers in all cases
     * @param mix _args:  optional arguments to pass through as key+value pairs
     * @return data object or boolean false on failure; failures trigger console logs
    */
    callGetService(_service = '', _cb, _args = {}) {

        let params = [];
        for(let key in _args) {
            params.push(key + '=' + _args[key]);
        }
        params = params.join('&');
        
        axios.get("/services/" + _service + "?" + params)
            .then((response) => {
                if(response.data.success) {
                    _cb(response.data);
                }else {
                    console.log('Ah Crud! Something freakin\' snapped! Got not service response on "' + _service + '"!');
                    _cb(false);
                }
            })
            .catch( (e) => {
                console.log('Connection Issues on "' + _service + '"? Error: ' + e);
                _cb(false);
            })
    }
};

export default AxiosHelper;