/* See Confluence docs for detailed info @ /display/CDCX/CarsDirect+Custom+Modal+-+ModalBox */
window.ModalBox = (function() {
    var _self = this;
    /* Begin Editable Settings */
    _self.setting = {
    	obj : {
			_class : 'ModalBox',
			_id : 'ModalBox' /* this becomes unique on each instance of modal */
		},
		icon : {
			close : 'icon-remove',
			naviLeft : 'icon-angle-left',
			naviRight : 'icon-angle-right',
			pagerLeft : '\\f053',
			pagerRight : '\\f054'
		},
		modalWidth : '300px',
		modalHeight : '450px',
		modalTop : null,
		modalRight : null,
		modalBottom : null,
		modalLeft : null,
		showExit : true,
		showCover : true,
		exitOnCover : true,
		showLeftNavi : false,
		showRightNavi : false,
		showBodyNavi : true,
		resize : 'none',		/* both|vertical|horizontal|none */
		drag : false,
		dragData : {
			obj : null,
			startX : 0, startY : 0,
			selfX : 0, selfY : 0
		},
		/* these are for auto resizing */
		resizeHandler : null,
		autoResizeX : false,
		autoResizeY : false,
		modalOffsetWidth : 0,
		modalOffsetHeight : 0,
		lastViewportWidth : 0,
		lastViewportHeight : 0
	};
	/* End editable */
	
	_self.objects = {
		modal : null,
		titleBar : {
			obj : null,
			leftNavi : null,
			rightNavi : null,
			text : null,
			exit : null
		},
		body : null,
		cover : null,
		css : null,
		pagerLeft : null,
		pagerRight : null,
		currentPager : null,
	};
	// This is specific used when using pagination to know index before the close 
	_self.pagerIndex = null;
	_self.pagination = null;
	_self.paginationCallbackAfterLeftNavi;
	_self.paginationCallbackAfterRightNavi;
	
	_self.eventType = (navigator.userAgent.indexOf('iPad') != -1) ? 'touchstart' : 'click';
	_self.closeCallback;
	_self.leftNaviCallback;
	_self.rightNaviCallback;
	_self.onResizeCallback;
	_self.lastNodeContent;
	/**
	* Private Methods
	*/
	var setNodePlaceholder = function(obj) {
		var _parent = obj.parentNode, elem, objID;
		if(obj.id) {
			objID = obj.id;
		}else {
			objID = 'ModalBoxContentRef' + uniqueInstance();
			obj.setAttribute('id', objID);
		}
		elem = document.createElement('span');
		elem.setAttribute('id', objID + '-Placeholder');
		_parent.insertBefore(elem, obj);
		_self.lastNodeContent = obj;
	};
	var reloadNodeAndClear = function() {
		if(_self.lastNodeContent) {
			var placeholder = document.getElementById(_self.lastNodeContent.id + '-Placeholder');
			if(_self.lastNodeContent.id.indexOf('ModalBox') != -1) {
				_self.lastNodeContent.removeAttribute('id');
			}
			placeholder.parentNode.insertBefore(_self.lastNodeContent, placeholder);
			placeholder.parentNode.removeChild(placeholder);
			_self.lastNodeContent = null;
		}
		if(_self.objects.body) {
			_self.objects.body.innerHTML = '';
		}
	};
	var buildCover = function() {
		var markup = document.getElementById('ModalCover');
		if(!markup) {
			markup = document.createElement('div');
			markup.setAttribute('id', 'ModalCover');
			markup.setAttribute('class', 'ModalCover');
			if(!document.body) {
				_self.consoleLog('document.body doesn\'t yet exist; failed creating modal cover');
				return false;
			}
			document.body.appendChild(markup);
		}
		_self.objects.cover = markup;
	};
	var buildListeners = function() {
		_self.objects.titleBar.exit.addEventListener(_self.eventType, function(e) {
			_self.hide(_self.closeCallback);
			e.preventDefault();
            e.stopPropagation();
		}, false);
		_self.objects.titleBar.leftNavi.addEventListener(_self.eventType, function(e) {
			if(_self.leftNaviCallback) {
				_self.leftNaviCallback(_self, e);
			}
		}, false);
		_self.objects.titleBar.rightNavi.addEventListener(_self.eventType, function(e) {
			if(_self.rightNaviCallback) {
				_self.rightNaviCallback(_self, e);
			}
		}, false);
		_self.objects.cover.addEventListener(_self.eventType, function(e) {
            if(_self.setting.exitOnCover) {
                _self.hide(_self.closeCallback);
			}
			e.preventDefault();
            e.stopPropagation();
		}, false);
        window.addEventListener('orientationchange', function(e) {
            _self.setPosition();
        }, false);
	};
	var buildCSS = function() {
		var css = ''
			+ '.' + _self.setting.obj._class + '{'
			+ 'position:absolute;display:none;min-width:' + _self.setting.modalWidth + ';width:' + _self.setting.modalWidth + ';'
			+ 'min-height:' + _self.setting.modalHeight + ';margin:0 auto;z-index:9999;background-color:#fff;'
			+ 'overflow:hidden;resize:' + _self.setting.resize +';}'
			+ '.' + _self.setting.obj._class + ' .titleBar{'
			+ 'position:relative;box-sizing:border-box;padding:10px;font-weight:bold;min-height:40px;text-align:center;color:#66615b;background-color:#f5f5f5;border-bottom:solid 1px #efefef;}'
			+ '.' + _self.setting.obj._class + ' .titleBarLeftNavi{position:absolute;display:none;top:15%;left:10px;line-height:26px;font-size:26px;cursor:pointer;}'
			+ '.' + _self.setting.obj._class + ' .titleBarRightNavi{position:absolute;display:none;top:15%;right:10px;line-height:26px;font-size:26px;cursor:pointer;}'
			+ '.' + _self.setting.obj._class + ' .titleBarExit{'
			+ 'position:absolute;right:10px;top:35%;font-family:FontAwesome;line-height:14px;font-size:14px;}'
			+ '.' + _self.setting.obj._class + ' .contentBody{'
			+ 'padding:15px;text-align:left;font-size:14px;}'
			+ '.ModalCover{position:fixed;display:none;top:0;left:0;bottom:0;width:100%;background-color:rgba(0,0,0,0.5);z-index:9998;}'
			+ '.' + _self.setting.obj._class + ' .ModalBoxPage{display:none;border:dotted 1px #c0c0c0;}'
			+ '.' + _self.setting.obj._class + ' .ModalBoxPageActive{display:block;}'
			+ '.' + _self.setting.obj._class + ' .ModalBoxPagerR,.ModalBoxPagerL{position:absolute;top:50%;z-index:9997;padding:5px;color:#808080;font-size:30px;font-family:FontAwesome;opacity:0.35;cursor:pointer;}'
			+ '.' + _self.setting.obj._class + ' .ModalBoxPagerL{left:0;display:none;}'
			+ '.' + _self.setting.obj._class + ' .ModalBoxPagerR{right:0;}'
			+ '.' + _self.setting.obj._class + ' .ModalBoxPagerL::before{content:"' + _self.setting.icon.pagerLeft + '";}'
			+ '.' + _self.setting.obj._class + ' .ModalBoxPagerR::before{content:"' + _self.setting.icon.pagerRight + '";}';
			
		var st = document.createElement('style');
			st.setAttribute('type', 'text/css');
			st.setAttribute('id', 'ModalWindowStyle');
			st.innerHTML = css;
		var header = document.head || document.getElementsByTagName('head')[0];
        // see if there's a skin; if so, append before it
        var hasSkin = document.getElementsByClassName('ModalBoxSkin')[0];
        if(hasSkin) {
            header.insertBefore(st, hasSkin);
        }else {
            header.appendChild(st);
        }
		_self.objects.css = st;
	};
	var buildMarkup = function() {
		if(!_self.objects.css) {
			buildCSS();
		}
		var markup = '', wrapper, titleBar, titleBarText, titleBarExit, content;
		wrapper = document.createElement('div');
		wrapper.setAttribute('id', _self.setting.obj._id + uniqueInstance());
		wrapper.setAttribute('class', _self.setting.obj._class);
		if(_self.setting.drag) {
			wrapper.setAttribute('draggable', true);
		}
		wrapper.setAttribute('data-originw', 0);
		wrapper.setAttribute('data-originh', 0);
		wrapper.setAttribute('data-originx', 0);
		wrapper.setAttribute('data-originy', 0);
		
		titleBar = document.createElement('div');
		titleBar.setAttribute('class', 'titleBar');
		titleBarLeftNavi = document.createElement('span');
		titleBarLeftNavi.setAttribute('class', 'titleBarLeftNavi ' + _self.setting.icon.naviLeft);
		titleBarRightNavi = document.createElement('span');
		titleBarRightNavi.setAttribute('class', 'titleBarRightNavi ' + _self.setting.icon.naviRight);
		titleBarText = document.createElement('span');
		titleBarText.setAttribute('class', 'titleBarText');
		titleBarExit = document.createElement('span');
		titleBarExit.setAttribute('class', 'titleBarExit ' + _self.setting.icon.close);
		if(_self.eventType == 'click') {
			titleBarExit.setAttribute('title', 'Click to exit');
			titleBarExit.style.cursor = 'pointer';
		}
		titleBar.appendChild(titleBarLeftNavi);
		titleBar.appendChild(titleBarRightNavi);
		titleBar.appendChild(titleBarText);
		titleBar.appendChild(titleBarExit);
		
		content = document.createElement('div');
		content.setAttribute('class', 'contentBody');
		
		wrapper.appendChild(titleBar);
		wrapper.appendChild(content);
		if(!document.body) {
			_self.consoleLog('document.body doesn\'t yet exist; failed creating modal');
			return false;
		}
		document.body.appendChild(wrapper);
		_self.objects.body = content;
		_self.objects.titleBar.obj = titleBar;
		_self.objects.titleBar.leftNavi = titleBarLeftNavi;
		_self.objects.titleBar.rightNavi = titleBarRightNavi;
		_self.objects.titleBar.text = titleBarText;
		_self.objects.titleBar.exit = titleBarExit;
		_self.objects.modal = wrapper;
		buildCover();
		buildListeners();
	};
	var checkOptions = function(option) {
        // Are we skinning?
        if(option.skin && option.skin !== '') {
            var skinRef = 'ModalBoxSkin_' + option.skin;
			_self.objects.modal.classList.add(skinRef);
            // If skin doesn't already exist, include it
            if(!document.getElementById(skinRef)) {
                _self.loadSkin(option.skin);
            }
        }
        // This appends a second class name next to 'ModalBox' class but with a 'ModalBox_' as prepended
        if(option.customClass) {
            if(option.customClass !== '') {
                _self.objects.modal.classList.add('ModalBox_' + option.customClass);
            }
        }
		if(option.beforeCallback) {
			if(isObject(option.beforeCallback)) {
				option.beforeCallback();
			}
		}
		if(typeof option.css === 'object') {
			var i, cssCnt = option.css.length;
			for(i = 0; i < cssCnt; i ++) {
				_self.objects.css.innerHTML += option.css[i];
			}
		}
		if(option.leftNaviCallback) {
			if(isObject(option.leftNaviCallback)) {
				_self.leftNaviCallback = option.leftNaviCallback;
			}
		}
		if(option.rightNaviCallback) {
			if(isObject(option.rightNaviCallback)) {
				_self.rightNaviCallback = option.rightNaviCallback;
			}
		}
		if(option.autoResizeY && _self.objects.modal) {
		    _self.setting.autoResizeY = option.autoResizeY;
		}
		if(option.autoResizeX && _self.objects.modal) {
		    _self.setting.autoResizeX = option.autoResizeX;
		}
		if(_self.setting.autoResizeX || _self.setting.autoResizeY) {
		    _self.setting.resizeHandler = window.addEventListener('resize', autoResizeListener, false);
		}
		if(option.onResizeCallback) {
		    if(isObject(option.onResizeCallback)) {
		        _self.onResizeCallback = option.onResizeCallback;
		    }
		}
		if(option.title) {
			_self.objects.titleBar.text.innerHTML = '';
			if(typeof option.title === 'object') {
				_self.objects.titleBar.text.appendChild(option.title);
			}else {
				_self.objects.titleBar.text.innerHTML = option.title;
			}
			if(option.titleCallback) {
				if(isObject(option.titleCallback)) {
					option.titleCallback(_self.objects.titleBar.text);
				}
			}
		}
        if(typeof option.showTitleBar === 'boolean') {
            _self.objects.titleBar.obj.style.display = (!option.showTitleBar) ? 'none' : 'block';
        }
		if(typeof option.showBodyNavi != 'undefined') {
			_self.setting.showBodyNavi = option.showBodyNavi;
		}
		if(option.body) {
			/* If this is a indexed array, let's special handle it */
			if(Array.isArray(option.body)) {
				_self.buildPagedBody(option.body);
			}else {
				reloadNodeAndClear();
				if(typeof option.body === 'object') {
					setNodePlaceholder(option.body);
					_self.objects.body.appendChild(option.body);
				}else {
					_self.objects.body.innerHTML = option.body;
				}
			}
			if(option.bodyCallback) {
				if(isObject(option.bodyCallback)) {
					option.bodyCallback(_self.objects.body);
				}
			}
		}
		if(typeof option.showExit != 'undefined') {
			_self.setting.showExit = (option.showExit) ? true : false;
			if(!_self.setting.showExit) {
				_self.objects.titleBar.exit.style.display = 'none';
			}else {
                _self.objects.titleBar.exit.style.display = 'block';
            }
		}
		if(option.width) {
			_self.objects.modal.style.width = (isNaN(option.width)) ? option.width : option.width + 'px';
		}
        if(option.maxWidth) {
            _self.objects.modal.style.maxWidth = (isNaN(option.maxWidth)) ? option.maxWidth : option.maxWidth + 'px';
        }
		if(option.height) {
			_self.objects.modal.style.height = (isNaN(option.height)) ? option.height : option.height + 'px';
            _self.objects.modal.style.minHeight = _self.objects.modal.style.height;
		}
        if(option.minHeight) {
            _self.objects.modal.style.minHeight = (isNaN(option.minHeight)) ? option.minHeight : option.minHeight + 'px';
        }
        if(option.maxHeight) {
            _self.objects.modal.style.maxHeight = (isNaN(option.maxHeight)) ? option.maxHeight : option.maxHeight + 'px';
        }
        if(option.top == 'auto') { /* we reset our top & have the modal determine it */
            _self.setting.modalTop = null;
        }else if(option.top || option.top === 0) {
			_self.setting.modalTop = (isNaN(option.top)) ? option.top : option.top + 'px';
			_self.objects.modal.style.top = _self.setting.modalTop;
		}
		if(option.right || option.right === 0) {
			_self.setting.modalRight = (isNaN(option.right)) ? option.right : option.right + 'px';
			_self.objects.modal.style.right = _self.setting.modalRight;
		}
		if(option.bottom || option.bottom === 0) {
			_self.setting.modalBottom = (isNaN(option.bottom)) ? option.bottom : option.bottom + 'px';
			_self.objects.modal.style.bottom = _self.setting.modalBottom;
		}
		if(option.left || option.left === 0) {
			_self.setting.modalLeft = (isNaN(option.left)) ? option.left : option.left + 'px';
			_self.objects.modal.style.left = _self.setting.modalLeft;
		}
		if(option.closeCallback) {
			if(isObject(option.closeCallback)) {
				_self.closeCallback = option.closeCallback;
			}
		}
		if(typeof option.showLeftNavi != 'undefined') {
			_self.objects.titleBar.leftNavi.style.display = (option.showLeftNavi) ? 'block' : 'none';
			_self.setting.showLeftNavi = option.showLeftNavi;
		}else {
			_self.objects.titleBar.leftNavi.style.display = _self.setting.showLeftNavi;
		}
		if(typeof option.showRightNavi != 'undefined') {
			if(option.showRightNavi) {
				_self.objects.titleBar.exit.style.display = 'none';
				_self.setting.showExit = false;
				_self.objects.titleBar.rightNavi.style.display = 'block';
			}else {
				_self.objects.titleBar.rightNavi.style.display = 'none';
			}
			_self.setting.showRightNavi = option.showRightNavi;
		}else {
			_self.objects.titleBar.rightNavi.style.display = _self.setting.showRightNavi;
		}
		if(option.padding || option.padding === 0) {
            _self.objects.body.style.padding = (isNaN(option.padding)) ? option.padding : option.padding + 'px';
        }
        if(option.drag) {
            _self.setting.drag = true;
        }
		if(option.resize) {
			_self.setting.resize = option.resize;
			_self.objects.modal.style.resize = _self.setting.resize;
		}
		if(typeof option.exitOnCover != 'undefined') {
			_self.setting.exitOnCover = (option.exitOnCover) ? true : false;
		}
        if(typeof option.showCover != 'undefined') {
			_self.setting.showCover = (option.showCover) ? true : false;
		}
		if(typeof option.pagination != 'undefined') {
            _self.pagination = (option.pagination) ? true : false;
        }
        if(typeof option.paginationCallbackBeforeLeftNavi === 'function') {
            _self.paginationCallbackBeforeLeftNavi = option.paginationCallbackBeforeLeftNavi;
        }
        if(typeof option.paginationCallbackBeforeRightNavi === 'function') {
            _self.paginationCallbackBeforeRightNavi = option.paginationCallbackBeforeRightNavi;
        }
        if(typeof option.paginationCallbackAfterLeftNavi === 'function') {
            _self.paginationCallbackAfterLeftNavi = option.paginationCallbackAfterLeftNavi;
        }
        if(typeof option.paginationCallbackAfterRightNavi === 'function') {
            _self.paginationCallbackAfterRightNavi = option.paginationCallbackAfterRightNavi;
        }
	};
	var resizeWidth = function(w) {
	    let newWidth;
	    if(typeof w != 'undefined' && w !== '') {
	        newWidth = w;
	    }else {
	        newWidth = window.innerWidth - _self.setting.modalOffsetWidth;
	    }
        _self.objects.modal.style.width = newWidth + 'px';
        // any callbacks
        if(_self.onResizeCallback) {
            _self.onResizeCallback(_self, newWidth, null);
        }
        return newWidth;
	};
	var resizeHeight = function(h) {
	    let newHeight;
	    if(typeof h != 'undefined' && h !== '') {
	        newHeight = h;
	    }else {
	        newHeight = document.documentElement.clientHeight - _self.setting.modalOffsetHeight;
	    }
        _self.objects.modal.style.minHeight = newHeight + 'px';
        _self.objects.modal.style.height = newHeight + 'px';
        // any callbacks
        if(_self.onResizeCallback) {
            _self.onResizeCallback(_self, null, newHeight);
        }
        return newHeight;
	};
	var autoResizeListener = function(e) {
	    if(!_self.objects.modal) {
	        return false;
	    }
	    let offsetWidth = document.documentElement.clientWidth - _self.objects.modal.clientWidth;
	    let newWidth = _self.objects.modal.clientWidth;
	    if(_self.setting.autoResizeX && offsetWidth != _self.setting.modalOffsetWidth) {
            newWidth = resizeWidth();
        }
	    let offsetHeight = document.documentElement.clientHeight - _self.objects.modal.clientHeight;
	    let newHeight = _self.objects.modal.clientHeight;
	    if(_self.setting.autoResizeY && offsetHeight != _self.setting.modalOffsetHeight) {
	        newHeight = resizeHeight();
        }
        setPosition();
        if((_self.setting.autoResizeX || _self.setting.autoResizeY) && _self.onResizeCallback) {
            _self.onResizeCallback(this, newWidth, newHeight);
        }
	};
	var isObject = function(obj) {
		return (typeof obj == 'function' || obj == 'object') ? true : false;
	};
	var uniqueInstance = function() {
		return '-' + (Math.round(Math.random() * 99999) + 1);
	};
	/**
	* Public Methods
	*/
    /* Load skin */
    this.loadSkin = function(skin, pth) {
        var skinId = 'ModalBoxSkin_' + skin;
        // don't load it again
        if(document.getElementById(skinId)) {
            return;
        }

        if(typeof pth != 'string' || pth === '') {
            pth = '//cdcssl.ibsrv.net/cdcx/css';
        }
        var head = document.getElementsByTagName('head')[0],
		template = document.createElement('link');
		template.setAttribute('rel', 'stylesheet');
        template.setAttribute('class', 'ModalBoxSkin');
		template.setAttribute('id', 'ModalBoxSkin_' + skin);
		template.setAttribute('href', pth + '/ModalBoxSkin_' + skin + '.css');
        template.onload = function() {
            /*console.log('ModalBox:: css skin: "' + skin + '" fully loaded');*/
        };
		head.appendChild(template);
    };
	/* Appends new title content */
	this.setTitle = function(title, cb) {
		if(!_self.objects.modal) {
			buildMarkup();
		}
		if(typeof title === 'object') {
			_self.objects.titleBar.text.appendChild(title);
		}else {
			_self.objects.titleBar.text.innerHTML = title;
		}
		if(typeof cb == 'function' || typeof cb == 'object') {
			cb(_self.objects.titleBar.text);
		}
	};
	/* Appends new body content */
	this.setBody = function(content, cb) {
		if(!_self.objects.modal) {
			buildMarkup();
		}
		/* If this is a indexed array, let's special handle it */
		if(Array.isArray(content)) {
			_self.buildPagedBody(content);
		}else {
			if(typeof content === 'object') {
				_self.objects.body.appendChild(content);
			}else {
				if(_self.objects.body.innerHTML !== '') {
					_self.objects.body.innerHTML += content;
				}else {
					_self.objects.body.innerHTML = content;
				}
			}
		}
		if(typeof cb == 'function' || typeof cb == 'object') {
			cb(_self.objects.body);
		}
	};
	this.buildPagedBody = function(bodies) {
		var i, page, cnt = bodies.length, collection = [], naviL, naviR;
		for(i = 0; i < cnt; i ++) {
			page = document.createElement('div');
			page.setAttribute('class', 'ModalBoxPage' + (i == 0 ? ' ModalBoxPageActive' : ''));
			page.setAttribute('data-page', i);
			page.setAttribute('data-active', (i == 0 ? '1' : '0'));
			if(typeof bodies[i] == 'object') {
				page.appendChild(bodies[i]);
			}else {
				page.innerHTML = bodies[i];
			}
			_self.objects.body.appendChild(page);
		}
		/* Build navi */
		naviL = document.createElement('span');
		naviL.setAttribute('class', 'ModalBoxPagerL');
		naviL.addEventListener(_self.eventType, function(e) {
			_self.pager(this, e);
		}, false);
		naviR = document.createElement('span');
		naviR.setAttribute('class', 'ModalBoxPagerR');
		naviR.addEventListener(_self.eventType, function(e) {
			_self.pager(this, e);
		}, false);
		_self.objects.modal.appendChild(naviL);
		_self.objects.modal.appendChild(naviR);
		_self.objects.pagerLeft = naviL;
		_self.objects.pagerRight = naviR;
		// Should we hide our body navi?
		_self.objects.pagerLeft.style.display = (_self.setting.showBodyNavi) ? 'block' : 'none';
		_self.objects.pagerRight.style.display = (_self.setting.showBodyNavi) ? 'block' : 'none';
	};
	this.show = function(option) {
		if(typeof option != 'object' || option === '') {
			option = {};
		}
		if(!_self.objects.modal) {
			buildMarkup();
		}
		checkOptions(option);
		
		_self.showCover();
		_self.objects.modal.style.display = 'block';
		_self.setPosition();
		// get our difference between total width/height of Modal and it's viewport 
		//_self.setting.lastViewportWidth = document.documentElement.clientWidth;
		_self.setting.lastViewportWidth = window.innerWidth;
		_self.setting.modalOffsetWidth = _self.setting.lastViewportWidth - _self.objects.modal.clientWidth;
		_self.setting.lastViewportHeight = document.documentElement.clientHeight;
		_self.setting.modalOffsetHeight = _self.setting.lastViewportHeight - _self.objects.modal.clientHeight;
		
		if(option.afterCallback) {
			if(isObject(option.afterCallback)) {
				option.afterCallback(this);
			}
		}
		// this returns the entire ModalBox object instance
		return this;
	};
	this.setSize = function(w, h) {
	    if(w) {
	        resizeWidth(w);
	    }
	    if(h) {
	        resizeHeight(h);
	    }
	    return this;
	};
	this.setPosition = function() {
	    if(!_self.objects.modal) {
	        return false;
	    }
		var viewportH = document.documentElement.clientHeight, viewportW = document.documentElement.clientWidth,
			modalW = _self.objects.modal.offsetWidth, modalH = _self.objects.modal.offsetHeight,
            scrollOffset = (window.pageXOffset) ? window.pageXOffset : document.body.scrollTop + document.documentElement.scrollTop;
		/* only required one to set */
		_self.objects.modal.style.top = (_self.setting.modalTop) ? _self.setting.modalTop : (((viewportH / 2) - (modalH / 2)) + scrollOffset) + 'px';
		/* these are optional */
		if(_self.setting.modalLeft) {
			_self.objects.modal.style.left = _self.setting.modalLeft;
		}
		if(_self.setting.modalRight) {
			_self.objects.modal.style.right = _self.setting.modalRight;
		}
		if(!_self.setting.modalLeft && !_self.setting.modalRight) {
			_self.objects.modal.style.left = '0';
			_self.objects.modal.style.right = '0';
			/* F*king ipads can't do margin auto on absolute element overlays. */
			if(navigator.userAgent.toLowerCase().indexOf('ipad') != -1) {
			    /* we use window.innerWidth to get a DOM window representation (independent of viewport meta tag) */
			    if(_self.objects.modal.clientWidth < window.innerWidth) {
			        let diffWidth = (window.innerWidth - _self.objects.modal.clientWidth) / 2;
			        _self.objects.modal.style.left = diffWidth + 'px';
			        _self.objects.modal.style.right = diffWidth + 'px';
			    }
			}
		}
		if(_self.setting.modalBottom) {
			_self.objects.modal.style.bottom = _self.setting.modalBottom;
		}
		
		_self.objects.modal.setAttribute('data-originw', modalW);
		_self.objects.modal.setAttribute('data-originh', modalH);
		_self.objects.modal.setAttribute('data-originx', _self.objects.modal.offsetLeft);
		_self.objects.modal.setAttribute('data-originy', _self.objects.modal.offsetTop);
	};
	this.pager = function(t, e) {
		var pages = _self.objects.modal.getElementsByClassName('ModalBoxPage');
		var i, isActive, nextIndex, cnt = pages.length;
		for(i = 0; i < cnt; i ++) {
			if(pages[i].getAttribute('data-active') == '1') {
				isActive = i;
				pages[i].setAttribute('data-active', '0');
				pages[i].setAttribute('class', pages[i].getAttribute('class').replace(' ModalBoxPageActive', ''));
				break;
			}
		}
		if(t.className.indexOf('ModalBoxPagerR') != -1) {
			nextIndex = isActive + 1;
			nextIndex = (typeof pages[nextIndex] != 'undefined' ? nextIndex : 0);
		}else {
			nextIndex = isActive - 1;
			nextIndex = (typeof pages[nextIndex] != 'undefined' ? nextIndex : (cnt - 1));
		}
		pages[nextIndex].setAttribute('class', pages[nextIndex].getAttribute('class') + ' ModalBoxPageActive');
		pages[nextIndex].setAttribute('data-active', '1');
		_self.objects.pagerLeft.style.display = (nextIndex == 0) ? 'none' : 'block';
		_self.objects.pagerRight.style.display = (nextIndex == (cnt - 1)) ? 'none' : 'block';
		
		if(t.className.indexOf('ModalBoxPagerR') != -1) {
    		if( typeof _self.paginationCallbackAfterRightNavi === "function"){
                _self.paginationCallbackAfterRightNavi(t, isActive);
            }
		}else{
    		if( typeof _self.paginationCallbackAfterLeftNavi === "function"){
                _self.paginationCallbackAfterLeftNavi(t, isActive);
            }
		}
	};
	this.hide = function(cb) {
	    try {
	        window.removeEventListener('resize', autoResizeListener, false);
	        _self.setting.resizeHandler = null;
	    }catch(er) {}
	    
	    if(_self.pagination){
    	    _self.objects.currentPager = _self.objects.modal.getElementsByClassName('ModalBoxPageActive')[0];
    		_self.pagerIndex = (_self.objects.currentPager) ? _self.objects.currentPager.getAttribute('data-page') : null;
	    }
		reloadNodeAndClear();
		if(_self.objects.cover) {
			_self.objects.cover.style.display = 'none';
		}
		_self.objects.modal.style.display = 'none';
		if(typeof cb == 'function' || typeof cb == 'object') {
		    if(_self.pagination){
			    cb(_self.objects.modal , _self.pagerIndex,  _self.objects.currentPager);
		    }else{
		        cb(_self.objects.modal);
		    }
		}
	};
	this.destroy = function(cb) {
	    try {
	        window.removeEventListener('resize', autoResizeListener, false);
	        _self.setting.resizeHandler = null;
	    }catch(er) {}
	    
		if(_self.objects.modal) {
			_self.objects.modal.parentNode.removeChild(_self.objects.modal);
			_self.objects.css.parentNode.removeChild(_self.objects.css);
		}
		if(_self.objects.cover) {
			_self.objects.cover.parentNode.removeChild(_self.objects.cover);
		}
		for(var i in _self.objects) {
			if(i == 'titleBar') {
				for(var a in _self.objects[i]) {
					_self.objects[i][a] = null;
				}
			}else {
				_self.objects[i] = null;
			}
		}
		if(typeof cb == 'function' || typeof cb == 'object') {
		    if(_self.pagination){
			    cb(_self.objects.modal , _self.pagerIndex,  _self.objects.currentPager);
		    }else{
		        cb(_self.objects.modal);
		    }
		}
	};
	this.showCover = function(bool) {
        if(!_self.setting.showCover) {
            return false;
        }
		bool = (typeof bool === 'boolean') ? bool : true;
		_self.objects.cover.style.display = (bool) ? 'block' : 'none';
	};
	this.describeObjects = function() {
		var i, a, detail = {
			modal : 'The entire Modal object',
			titleBar : {
				obj : 'The entire Modal title bar element and children',
				leftNavi : 'The left arrow navi in Modal title bar',
				rightNavi : 'The right arrow navi in Modal title bar; if shown, will hide exit icon',
				text : 'The text (title) portion of Modal title bar',
				exit : 'The exit icon of Modal title bar'
			},
			body : 'The entire content area of Modal, except for title bar element',
			cover : 'The Modal background cover',
			css : 'The Style sheet associated to Modal; built to header',
			pagerLeft : 'If multiple Modal box body content, this will page through them backward',
			pagerRight : 'If multiple Modal box body content, this will page through them forward'
		};
		var info = [];
		for(i in _self.objects) {
			if(typeof detail[i] == 'object') {
				for(a in _self.objects[i]) {
					info.push('\t\t' + i + '.' + a + ' : ' + detail[i][a]);
				}
			}else {
				info.push('\t\t' + i + ' : ' + detail[i]);
			}
		}
		_self.consoleLog("Objects belonging to the ModalBox.objects: \n" + info.join("\n"));
	};
	this.consoleLog = function(m, o) {
		if(window.console) {
			if(typeof o == 'object') {
				console.log('ModalBox :: ' + m, o);
			}else {
				console.log('ModalBox :: ' + m);
			}
		}
	};
	
	return this;
	
})();