
const JSUtilsMixin = {
    data() {
        return {
            JSUtils : {
                stringTool : {
                    isBlank : function(str) {
                    return (!str || /^\s*$/.test(str));
                    },
                    normalize : function(denormString) {
                    if(!denormString) {
                    denormString = "";
                    }
                    return denormString.toLowerCase().trim().replace('&amp;', '-').replace(/[^a-zA-Z0-9]+/g, '-').replace(/^-/g, '').replace(/-$/g, '');
                    }
                },
                numberTool : {
                    formatPrice(num, _decimal = false) {
                        let val = (_decimal) ? (num * 1).toFixed(2) : (num * 1).toFixed(0);
                        return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    }
                },
                consoleLog: function(msg, obj) {
                    if(typeof msg != 'string' || msg == '') {
                        return false;
                    }
                    if(typeof obj == 'object') {
                        console.log(msg, obj);
                    } else {
                        console.log(msg);
                    }
                    return true;
                }
            }
        }
    },
    methods : {
        
    }
}

export default JSUtilsMixin;