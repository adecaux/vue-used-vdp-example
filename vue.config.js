module.exports = {
    publicPath: process.env.VUE_APP_ROOT_PATH,
    devServer: {
        proxy: {
            "^/services": {
                target: "http://stg-new-car-services.carsdirect.com",
                ws: true,
                changeOrigin: true
            }
        }
    },
    runtimeCompiler : true
}